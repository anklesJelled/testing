(function() {
	var app = angular.module('store', []);



	app.controller('StoreController', function() {
		this.products = gems;
	});



	app.controller('PanelController', function() {
		// init tab so that description is shown on load
		this.tab = 1;

		// clicking a button sets the value of tab
		this.selectTab = function(setTab) {
			this.tab = setTab;
		};

		// show panel based on active selected tab
		this.isSelected = function(checkTab) {
			// return the value if they are equal : boolean
			return this.tab == checkTab;
		};
	});






	var gems = [
	{
		name: 'Dodecahedron',
		price: 2.95,
		description: 'This Dodecahedron gem is fantastic!!!',
		images: [
			{
				full: 'img/gem-01.gif',
				thumb: 'img/gem-01-t.gif',
			}
		],
		canPurchase: true,
		soldOut: true,
	},
	{
		name: 'Pentagonal Gem',
		price: 5.95,
		description: 'This is a Pentagonal gem. It is great.',
		images: [
			{
				full: 'img/gem-02.gif',
				thumb: 'img/gem-02-t.gif',
			}
		],
		canPurchase: true,
		soldOut: false,
	}
	];


// Experimentation ~K

	app.controller('OtherController', function() {
		this.family = person;
	});

	var person = {
		first: 'Bob',
		last: 'Jones',
	}
})();
